<?php
include("framework/core/loader.php");

try {
  \Raphael\Core\Loader::Initialize();

  $config = new \Raphael\Core\Config;
  $config->Set("dirs", array(
    "appDir" => "app",
    "viewsDir" => "views",
    "modelsDir" => "models",
    "controllersDir" => "controllers"
  ));

  $config->Set("database", new \Raphael\Database\MySQL(array(
    "host" => "localhost",
    "username" => "root",
    "password" => "root",
    "database" => "raphael"
  )));

  $config->Set("modules", array("request"));

  \Raphael\Core\Loader::Load($config);
  \Raphael\Core\Loader::Request();
} catch (Exception $e) {
  echo $e->getMessage();
}
