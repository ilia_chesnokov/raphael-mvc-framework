<?php

namespace Raphael\Database;

class MySQL {
  private $link;
  private $config;

  public function __construct($data) {
    /*
     * Data:
     * host, user, password, database, port, socket
     */

    $this->config = $data;
  }

  public function Connect() {
     $data = $this->config;

     $this->link = @mysqli_connect($data['host'],
                                   $data['username'],
                                   $data['password'],
                                   $data['database'],
                                   (empty($data['port']) ? 3306 : $data['port']),
                                   (empty($data['socket']) ? "" : $data['socket']));
    if (mysqli_connect_errno()) {
      throw new \Exception(\Raphael\Core\Exceptions::GetExceptionString("DATABASE_CONNECT_ERROR"));
    }

    return $this->link;
  }

  public function Disconnect(mysqli $link) {
    mysqli_close($link);
  }

  // Queries
  public function Query($request) {
    $result = @mysqli_query($this->link, $request);

    if (@mysqli_errno($this->link)) {
      throw new \Exception(\Raphael\Core\Exceptions::GetExceptionString("BAD_QUERY", mysqli_error($this->link)));
      return false;
    }

    return $result;
  }

  public function NumRows($query) {
    $result = @mysqli_num_rows($query);

    return $result;
  }

  public function Fetch($query) {
    $result = @mysqli_fetch_assoc($query);

    return $result;
  }
}
