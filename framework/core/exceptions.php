<?php
namespace Raphael\Core;

class Exceptions {
  private static $exceptions = array("DATABASE_CONNECT_ERROR" => "Raphael can't connect with DB",
                                     "BAD_DIR" => "Raphael can't read dir %x",
                                     "CANT_OPEN_DIR" => "Raphael can't open dir %x",

                                     "BAD_CONFIG_PARAMETER" => "Config doesn't have parameter %x",
                                     "BAD_QUERY" => "Bad MySQL Query. %x",

                                     "CONTROLLER_DOESNT_EXISTS" => "Controller %x doesn't exists",
                                     "CLASS_DOESNT_EXISTS" => "Class %x doesn't exists",
                                     "ACTION_DOESNT_EXISTS" => "Action %x doesn't exists",
                                     "MODULE_DOESNT_EXISTS" => "Module %x doesn't exists");

  static public function GetExceptionString($exception, $other = null) {
    if (isset(self::$exceptions[$exception])) {
      return (empty($other) ? self::$exceptions[$exception] : str_replace("%x", $other, self::$exceptions[$exception]));
    }
  }
}
