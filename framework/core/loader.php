<?php
namespace Raphael\Core;

include_once("framework/core/exceptions.php");
include_once("framework/modules/files.php");

abstract class RaphaelFramework {
  public static $database;
  public static $modules;
  private static $config;

  // Loader operations
  public static function SetConfig($config) {
    self::$config = $config;
  }

  public static function GetControllersDirAddress() {
    return self::$config->GetControllersDirAddress();
  }
}

class Config {
  private $config = array();

  public function Set($key, $value) {
    $this->config[$key] = $value;
  }

  public function CheckDirs() {
    if (!isset($this->config['dirs'])) {
      throw new \Exception(\Raphael\Core\Exceptions::GetExceptionString("BAD_CONFIG_PARAMETER", "dirs"));
    } if (empty($this->config['dirs']['appDir'])) {
      throw new \Exception(\Raphael\Core\Exceptions::GetExceptionString("BAD_CONFIG_PARAMETER", "dirs/appDir"));
    } if (empty($this->config['dirs']['viewsDir'])) {
      throw new \Exception(\Raphael\Core\Exceptions::GetExceptionString("BAD_CONFIG_PARAMETER", "dirs/controllersDir"));
    } if (empty($this->config['dirs']['modelsDir'])) {
      throw new \Exception(\Raphael\Core\Exceptions::GetExceptionString("BAD_CONFIG_PARAMETER", "dirs/modelsDir"));
    } if (empty($this->config['dirs']['controllersDir'])) {
      throw new \Exception(\Raphael\Core\Exceptions::GetExceptionString("BAD_CONFIG_PARAMETER", "dirs/controllersDir"));
    }
  }

  public function GetDatabase() {
    return $this->config['database'];
  }

  public function GetModules() {
    return $this->config['modules'];
  }

  public function GetModelsDirAddress() {
    return "{$this->config['dirs']['appDir']}/{$this->config['dirs']['modelsDir']}";
  }

  public function GetControllersDirAddress() {
    return "{$this->config['dirs']['appDir']}/{$this->config['dirs']['controllersDir']}";
  }

  public function GetModelsList() {
    try {
      $models = \Raphael\Modules\Files::ReadDir("{$this->config['dirs']['appDir']}/{$this->config['dirs']['modelsDir']}");
    } catch (\Exception $e) {
      throw $e;
    }

    return $models;
  }
}

class Loader {
  public static function Initialize() {
    try {
      $database = \Raphael\Modules\Files::ReadDir("framework/database");
    } catch (\Exception $e) {
      throw $e;
    }

    foreach($database as $db) {
      include_once("framework/database/{$db}");
    }
  }

  public static function Load(Config $config) {
    $default = array("controller", "model", "view");
    foreach ($default as $structure) { include_once("framework/structures/{$structure}.php"); }

    try {
      $config->CheckDirs();
      $models = $config->GetModelsList();
    } catch (\Exception $e) {
      throw $e;
    }

    foreach ($models as $value) {
      include_once($config->GetModelsDirAddress() . "/{$value}");
    }

    foreach($config->GetModules() as $module) {
      if (!file_exists("framework/modules/{$module}.php")) {
        throw new \Exception(\Raphael\Core\Exceptions::GetExceptionString("MODULE_DOESNT_EXISTS", $module));
      }

      include_once("framework/modules/{$module}.php");
    }

    RaphaelFramework::$database = $config->GetDatabase();
    RaphaelFramework::setConfig($config);

    try {
      RaphaelFramework::$database->Connect();
    } catch (\Exception $e) {
      throw $e;
    }
  }

  public static function Controller($name, $action, $data) {
    $name = strtolower($name); $action = strtolower($action);

    if (!file_exists(RaphaelFramework::GetControllersDirAddress()."/{$name}Controller.php")) {
      throw new \Exception(\Raphael\Core\Exceptions::GetExceptionString("CONTROLLER_DOESNT_EXISTS", "{$name}Controller.php"));
    }

    include_once(RaphaelFramework::GetControllersDirAddress()."/{$name}Controller.php");

    if (!class_exists("{$name}Controller")) {
      throw new \Exception(\Raphael\Core\Exceptions::GetExceptionString("CLASS_DOESNT_EXISTS", "{$name}Controller"));
    }

    $objectName = "{$name}Controller";
    $object = new $objectName;

    if (!method_exists($object, "{$action}Action")) {
      throw new \Exception(\Raphael\Core\Exceptions::GetExceptionString("ACTION_DOESNT_EXISTS", "{$action}Action"));
    }
    $action = "{$action}Action";

    $object->$action($data);
  }

  public static function Request() {
    $url = @$_GET['__url'];
    $explode = explode("/", $url);

    if (empty($explode[0])) { $explode[0] = "index"; }
    if (empty($explode[1])) { $explode[1] = "index"; }
    if (empty($explode[2])) { $explode[2] = ""; }

    try {
      self::Controller($explode[0], $explode[1], $explode[2]);
    } catch (\Exception $e) {
      throw $e;
    }
  }
}
