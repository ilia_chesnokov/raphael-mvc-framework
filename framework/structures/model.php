<?php
namespace Raphael\Structures;

abstract class Model extends \Raphael\Core\RaphaelFramework {

  static public function Find($data = array()) {
    /*
     * Data:
     * Fields, Order, Limit
     * $data = array("fields" => array(""))
     */

     $class_name = get_called_class();
     $children = "\\{$class_name}";
     $object = new $children;

     $table = (method_exists($object, "tableName") ? $object->tableName() : $class_name);
      unset($object);

     if (!empty($data)) {
       $conditions = array();
       if (!empty($data['fields'])) {
         $conditions[0] = "WHERE ";
         foreach($data['fields'] as $key => $value) {
           $conditions[0] .= "$key = '{$value}',";
         }
         $conditions[0] = substr($conditions[0], 0, (strlen($conditions[0]) -1));
       } else {
         $conditions[0] = "";
       }

       if (isset($data['order'])) {
         $conditions[1] = "ORDER BY {$data['order']}";
       } else {
         $conditions[1] = "";
       }

       if (isset($data['limit'])) {
         $conditions[2] = "LIMIT {$data['limit']}";
       } else {
         $conditions[2] = "";
       }

       $query = "SELECT * FROM {$table} {$conditions[0]} {$conditions[1]} {$conditions[2]};";
     } else {
       $query = "SELECT * FROM {$table};";
     }

     $result = self::$database->Query($query);

     if ($result) {
       if (self::$database->NumRows($result)) {
         $objects_array = array();
         while ($fetch = self::$database->Fetch($result)) {
           $object[$class_name.$fetch['id']] = new $children;
           foreach($fetch as $key => $value) {
             $object[$class_name.$fetch['id']]->$key = $value;
           }

           $objects_array[] = $object[$class_name.$fetch['id']];
         }

         return $objects_array;
       } else {
         return array();
       }
     } else {
       return false;
     }
  }

  static public function FindById($id) {
    return self::Find(array("fields" => array("id" => $id)));
  }

  static public function FindFirst($fields = array()) {
    return self::Find(array("fields" => $fields, "limit" => "1"));
  }
}
