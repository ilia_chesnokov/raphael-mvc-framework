<?php
namespace Raphael\Modules;

class Request {
  static public function isPost() {
    return (!empty($_POST) ? true : false);
  }

  static public function isAjax() {
    return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH'] == 'xmlhttprequest') ? true : false);
  }

  static public function getPost() {
    return (empty($_POST) ? false : $_POST);
  }

  static public function get($name) {
    return (empty($_REQUEST[$name]) ? false : $_REQUEST[$name]);
  }
}
