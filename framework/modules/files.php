<?php
namespace Raphael\Modules;

class Files {
  static public function ReadDir($dir) {
    if (!is_dir($dir)) {
      throw new \Exception(\Raphael\Core\Exceptions::GetExceptionString("BAD_DIR"));
    }

    $files = array();
    if ($dh = @opendir($dir)) {
      while (($file = readdir($dh)) !== false) { if ($file != "." && $file != "..") { $files[] = $file; } }
    } else {
      throw new \Exception(\Raphael\Core\Exceptions::GetExceptionString("CANT_OPEN_DIR"));
    }

    return $files;
  }
}
